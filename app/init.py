# Import initilization-related python modules

# Import hardware modules
import app.modules.switch as switch

# Import components
import app.components.say as say

# Initialize app
def initialize():
    # Set global
    global switch, say

    # Initilize switch module
    switch = switch.module(12) # 12 = pin
    
    # Initialize say component
    say = say.component()

    # Do some initilization stuff
    print("IoT-Pi v1.1 initialized!")