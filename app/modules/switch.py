# import python modules
import pigpio

class module():
    def on(self):
        self.gpio.set_PWM_dutycycle(self.pin, 255)

    def off(self):
        self.gpio.set_PWM_dutycycle(self.pin, 0)

    def __init__(self, pin):
        self.pin = pin
        self.gpio = pigpio.pi()