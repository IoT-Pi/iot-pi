# Import and initilize app
from app import init as app
app.initialize()

# Other imports here
import time

# Actions here
app.say.echo("Hello...")
time.sleep(1)
app.say.echo("World")