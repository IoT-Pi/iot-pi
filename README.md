# IoT-Pi Framework

IoT-Pi is a framework which I've created to manage hardware components connected to your pi easier. Components are split up into modules. 

## Install & Run

1. Install required dependencies:
	```bash
	sudo apt-get install pigpio
	```

2. Clone the repository:
	```bash
	git clone https://gitlab.com/Niehaus_1301/iot-pi.git
	```

3. Navigate to the cloned repository
	```bash
	cd iot-pi/
	```

4. Activate the virtual python environment:
	```bash
	source iotpi/bin/activate
	```

5. Start pigpio daemon
	```bash
	sudo pigpiod
	```

5. Run the main python script
	```bash
	python3 main.py
	```

## Managing modules

Modules are stored in the `iotpi/modules/` directory. Make sure to import the module in the `iotpi/init.py` script as following:
```python
#Add imports from /modules here
#Import example module
import iotpi.modules.example as example
self.example = example.module()
```
>**Note:** Module path is `modules/example.py`